#ifndef THREADKEEPER_H
#define THREADKEEPER_H
#include "multithreaded_stack.h"

#include <thread>
#include <vector>
#include <string>

typedef void (*thread_func_t)(unsigned n, MultithreadedStack<std::string> &stack);

class ThreadKeeper {
    public:
    void make_new_thread(thread_func_t f, unsigned n, MultithreadedStack<std::string> &stack);
    ~ThreadKeeper();
    private:
    std::vector<std::thread> thread_;
};

#endif //THREADKEEPER_H