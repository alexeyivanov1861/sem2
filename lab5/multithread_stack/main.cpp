#include "threadkeeper.h"
#include "multithreaded_stack.h"

#include <iostream>
#include <fstream>
#include <string>

#define THREAD_COUNT    3u

void process_file(unsigned n, MultithreadedStack<std::string> &stack) {
    std::ifstream in("input" + std::to_string(n) + ".txt");
    std::ofstream out("output" + std::to_string(n) + ".txt");
    std::string str;
    char cmd;
    while (in >> cmd) {
        switch (cmd) {
            case '+':
                in >> str;
                stack.push_front(str);
                out << "pushed " << str << '\n';
                break;
            case '-':
                std::shared_ptr<std::string> p = stack.pop_front();
                if (!(p))
                    out << "popped null " << '\n';
                else
                    out << "popped " << *p << '\n';
                break;
        }
    }
}

int main() {
    MultithreadedStack<std::string> stack;
    ThreadKeeper keeper;
    for (unsigned i = 0; i < THREAD_COUNT; i++)
        keeper.make_new_thread(process_file, i + 1, std::ref(stack));
    return 0;
}