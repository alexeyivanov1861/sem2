#ifndef MULTITHREADED_STACK_H
#define MULTITHREADED_STACK_H

#include <stack>
#include <mutex>
#include <memory>

template <typename T>
class MultithreadedStack {
    public:
        void push_front(T a) {
            std::lock_guard<std::mutex> lock(m_);
            stack_.push(a);
        }
        std::shared_ptr<T> pop_front() {
            std::lock_guard<std::mutex> lock(m_);
            if (!stack_.empty()) {
                std::shared_ptr<T> tmp = std::make_shared<T>(stack_.top());
                stack_.pop();
                return tmp;
            } else {
                std::shared_ptr<T> tmp_null;
                return tmp_null;
            }
        }
    private:
        std::stack<T> stack_;
        std::mutex m_;
};

#endif //MULTITHREADED_STACK_H