#include "threadkeeper.h"

void ThreadKeeper::make_new_thread(thread_func_t f, unsigned n, MultithreadedStack<std::string> &stack) {
        std::thread t(f, n, std::ref(stack));
        thread_.push_back(std::move(t));
}

ThreadKeeper::~ThreadKeeper() {
        for (auto &it : thread_)
            it.join();
    }