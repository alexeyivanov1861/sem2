#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <utility>

void
get_matrix(std::ifstream &in, std::pair <int, int> size, std::vector <std::vector <char> > &a) {
	int i, j;
	for (i = 0; i < size.first; i++)
		for (j = 0; j < size.second; j++)
			in >> a.at(i).at(j);
}

void
bfs(const std::pair<int, int> &start, const std::pair <int, int> &size, std::vector <std::vector <char> > &a, std::vector <std::vector <char> > &vis) {
	std::queue <std::pair<int, int> > q;
	q.push(start);

	static const std::pair <int, int> side[4] = {
		std::make_pair(1, 0),
		std::make_pair(0, 1), 
		std::make_pair(-1, 0), 
		std::make_pair(0, -1) 
	};
	while (!q.empty()) {
		std::pair <int, int> cur(q.front());

		int i;
		for (i = 0; i < 4; i++) {
			std::pair <int, int> next;
			next.first = cur.first + side[i].first;
			next.second = cur.second + side[i].second;
			if (
				0 <= next.first && next.first < size.first &&
				0 <= next.second && next.second < size.second &&
				a.at(next.first).at(next.second) == '#' &&
				vis.at(next.first).at(next.second) == 0
			   ) {
				q.push(next);
				vis.at(next.first).at(next.second) = 1;
			}	
		}
		q.pop();
	}
}

int 
main() {
	std::ifstream in("input.txt");
	std::ofstream out("output.txt");

	std::pair<int, int> size;
	in >> size.first >> size.second;
	std::vector <std::vector <char> > a(size.first, std::vector <char>(size.second));
	std::vector <std::vector <char> > vis(size.first, std::vector <char>(size.second, 0));
	get_matrix(in, size, a);

	int count = 0;
	int i, j;
	for (i = 0; i < size.first; i++)
		for (j = 0; j < size.second; j++) {
			std::pair <int, int> start = std::make_pair(i, j);
			if  (
				a.at(start.first).at(start.second) == '#' &&
				vis.at(start.first).at(start.second) == 0
			    ) {
				bfs(start, size, a, vis);
				count++;
			}
		}
	out << count << std::endl;
	return 0;
}
