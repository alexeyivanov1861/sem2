#include <fstream>
#include <set>

int main() {
	std::ifstream in("input.txt");
	std::ofstream out("output.txt");

	std::set <int> s;

	int n;
	in >> n;

	int i;

	for (i = 0; i < n; i++) {
		int tmp;
		in >> tmp;
		s.insert(tmp);
	}

	in >> n;
	int j;
	for (i = 0; i < n; i++) {
		int count = 0;
		int k;
		in >> k;
		for (j = 0; j < k; j++) {
			int tmp;
			in >> tmp;
			if (s.find(tmp) != s.end())
				count++;
		}
		out << count << std::endl;
	}
	return 0;
}
