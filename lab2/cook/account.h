#include <string>

#ifndef ACCOUNT_H
#define ACCOUNT_H


class Account {
public:
    Account(const std::string& name);
    const std::string& name() const;
    bool operator<(const Account&) const;
private:
    std::string name_;
};

#endif // ACCOUNT_H
