#include <iostream>
#include <functional>
#include "database.h"

bool filter1(const Comment& c) {
    if (c.cook_name() == "Petya")
        return 1;
    return 0;
}

bool filter2(const Comment& c) {
    if (c.cook_name() == "Vanya")
        return 1;
    return 0;
}

void print_all_cooks(Database& database) {
	const std::set<Cook>& cooks = database.get_cooks();
	for (auto it = cooks.cbegin(); it != cooks.cend(); it++) {
		std::cout << it->name() << " : " << database.get_rating([&](const Comment& c){ return c.cook_name() == it->name();}) << '\n';
	}
}

int main() {
    Database database;
    database.add_cook(Cook("Petya"));
    database.add_cook(Cook("Vanya"));
    database.add_dish(Dish("Ice Cream", "Desert"));
    database.add_comment(Comment(Account("Fedor"), Dish("Ice Cream", "Desert"), Cook("Petya"), 5));
    database.add_comment(Comment(Account("Vanya"), Dish("Ice Cream", "Desert"), Cook("Vanya"), 3));
    double a = database.get_rating(filter1);
    double b = database.get_rating(filter2);
    std::cout << a << '\n' << b << '\n';
	print_all_cooks(database);
    return 0;
}
