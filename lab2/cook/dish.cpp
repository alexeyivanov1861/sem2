#include "dish.h"

Dish::Dish(const std::string& name, const std::string& type):
    name_(name), type_(type) {
}

bool Dish::operator<(const Dish& dish) const {
    return name_ < dish.name_;
}

const std::string& Dish::name() const {
	return name_;
}

const std::string& Dish::type() const {
	return type_;
}
