#include "cook.h"

Cook::Cook(const std::string& name) :
    name_(name) {
}

const std::string& Cook::name() const {
    return name_;
}

bool Cook::operator<(const Cook& cook) const {
    return name_ < cook.name_;
}
