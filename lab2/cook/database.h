#include "comment.h"
#include "dish.h"
#include "cook.h"
#include <set>
#include <map>
#include <vector>
#include <functional>

#ifndef DATABASE_H
#define DATABASE_H

typedef double (*avg_rating_t)(const std::vector<int>&);


class Database {
public:
    Database();
    void add_dish(const Dish&);
    void add_cook(const Cook&);
    void add_comment(const Comment&);
    double get_rating(std::function<bool(const Comment&)> f, avg_rating_t calculater = default_calculater) const;
    const std::set<Dish>& get_menu() const;
    const std::set<Cook>& get_cooks() const;
    static double default_calculater(const std::vector<int>&);
private:
    std::set<Dish> menu_;
    std::set<Cook> cooks_;
    std::map<Account, std::vector<Comment> > comments;
};

#endif // DATABASE_H
