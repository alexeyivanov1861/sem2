#include "comment.h"

Comment::Comment(const Account& author, const Dish& dish, const Cook& cook, int rating):
    author_(author), dish_(dish), cook_(cook), rating_(rating) {
}

const std::string& Comment::author_name() const {
    return author_.name();
}

const std::string& Comment::cook_name() const {
    return cook_.name();
}

const Account& Comment::author() const {
    return author_;
}

const Cook& Comment::cook() const {
    return cook_;
}

const Dish& Comment::dish() const {
    return dish_;
}

int Comment::rating() const {
    return rating_;
}
