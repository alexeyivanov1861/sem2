#include "cook.h"
#include <string>

#ifndef DISH_H
#define DISH_H


class Dish {
public:
    Dish(const std::string& name, const std::string& type);
    bool operator<(const Dish&) const;
	const std::string& name() const;
	const std::string& type() const;
private:
    std::string name_;
    std::string type_;
};

#endif // DISH_H
