#include "account.h"

Account::Account(const std::string& name) :
    name_(name) {
}

const std::string& Account::name() const {
    return name_;
}

bool Account::operator<(const Account& a) const {
    return name_ < a.name_;
}