#include <string>

#ifndef COOK_H
#define COOK_H

class Cook {
public:
    Cook(const std::string& name);
    const std::string& name() const;
    bool operator<(const Cook&) const;
private:
    std::string name_;
};

#endif // COOK_H
