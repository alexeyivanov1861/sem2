#include "database.h"
#include <iostream>

Database::Database() {
}

void Database::add_dish(const Dish& dish) {
    if (menu_.find(dish) == menu_.end())
        menu_.insert(dish);
}

void Database::add_cook(const Cook& cook) {
    if (cooks_.find(cook) == cooks_.end())
        cooks_.insert(cook);
}

void Database::add_comment(const Comment& comment) {
    if (cooks_.find(comment.cook()) != cooks_.end() &&
        menu_.find(comment.dish()) != menu_.end())
        comments[comment.author()].push_back(comment);
}

double Database::get_rating(std::function<bool(const Comment&)> f, avg_rating_t calculater) const {
    double result = 0;
    int c = 0;
    std::vector<int> client_marks;
    for (auto it = comments.begin(); it != comments.end(); it++) {
        for (auto j = it->second.begin(); j != it->second.end(); j++)
            if (f(*j))
                client_marks.push_back(j->rating());
        if (client_marks.size() > 0) {
            result += calculater(client_marks);
            client_marks.clear();
            c++;
       }
    }
    if (!c)
        return 0;
    return result / c;
}

const std::set<Dish>& Database::get_menu() const {
    return menu_;
}

const std::set<Cook>& Database::get_cooks() const {
    return cooks_;
}

double Database::default_calculater(const std::vector<int>& a) {
    double r = 0;
    for (int i = 0; i < a.size(); i++)
        r += a[i];
    r /= a.size();
    return r;
}