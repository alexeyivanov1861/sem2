#include "dish.h"
#include "account.h"
#include <string>
#include <vector>

#ifndef COMMENT_H
#define COMMENT_H


class Comment {
public:
    Comment(const Account& author, const Dish& dish, const Cook& cook, int rating);
    const std::string& author_name() const;
    const std::string& cook_name() const;
    const Account& author() const;
    const Cook& cook() const;
    const Dish& dish() const;
    int rating() const;
private:
    Account author_;
    const Dish dish_;
    const Cook cook_;
    int rating_;
};

#endif // COMMENT_H
