#ifndef __VECTOR3__
#define __VECTOR3__

#include <iostream>
#include <complex>
#include <type_traits>

template <typename T>
class Vector3 {
	public:
		static_assert(std::is_floating_point<T>::value, "Error: Type in Vector3 should be floating point!\n");
		Vector3(const T& x = 0,const T& y = 0,const T& z = 0):
			x_(x), y_(y), z_(z) {
		}

		Vector3<T> operator-() const {
			return Vector3<T>(-x_, -y_, -z_);
		}

		Vector3<T> operator+(const Vector3<T>& vec) const {
			return Vector3<T>(x_ + vec.x_, y_ + vec.y_, z_ + vec.z_);
		}

		Vector3<T> operator-(const Vector3<T>& vec) const {
			return *this + (-vec);
		}

		Vector3<T> operator*(const T a) const {
			return Vector3<T>(a * x_, a * y_, a * z_);
		}

		T operator*(const Vector3<T>& vec) const {
			return x_ * vec.x_ + y_ * vec.y_ + z_ * vec.z_;
		}

		Vector3<T> vec_product(const Vector3<T>& vec) const {
			return Vector3<T>(y_ * vec.z_ - z_ * vec.y_, z_ * vec.x_ - x_ * vec.z_, x_*vec.y_ - y_ * vec.x_);
		}


		T triple_product(const Vector3<T>& a, const Vector3<T>& b, const Vector3<T>& c) {
			return a.vec_product(b)*c;
		}

		T& operator[](int i) {
			if (i == 0) return x_;
			if (i == 1) return y_;
			return z_;
		}

		const T& operator[](int i) const {
			if (i == 0) return x_;
			if (i == 1) return y_;
			return z_;
		}

		template <typename T1>
		friend std::ostream& operator<<(std::ostream&, const Vector3<T1>&);

	private:
		T x_, y_, z_;
};

template <typename T>
Vector3<T> operator*(const T a, const Vector3<T> vec) {
	return vec * a;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const Vector3<T>& a) {
	out << a.x_ << " " << a.y_ << " " << a.z_;
	return out;
}
#endif // __VECTOR3__
