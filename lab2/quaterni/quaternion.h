#ifndef __QUATERNION__
#define __QUATERNION__

#include <iostream>
#include <complex>
#include "vector3.h"
#include <type_traits>

//header Quaternion
template <typename T>
class Quaternion {
	public:
		static_assert(std::is_floating_point<T>::value, "Error: Type in Quaternion should be floating point!\n");
		//constuctors
		Quaternion(const T& a = 0, const T& x = 0, const T& y = 0, const T& z = 0):
			a_(a), vec_(x, y, z) {
		}

		Quaternion(const T& a, const Vector3<T>& vec):
			a_(a), vec_(vec) {
		}

		Quaternion(const std::complex<T>& z1, const std::complex<T>& z2):
			a_(z1.real()), vec_(z1.imag(), z2.real(), z2.imag()) {
			}

		//operations
		Quaternion<T> operator-() const {
			return Quaternion(-a_, -vec_);
		}

		Quaternion<T> operator+(const Quaternion<T>& q) const {
			return Quaternion(a_ + q.a_, vec_ + q.vec_);
		}

		Quaternion<T> operator-(const Quaternion<T>& q) const {
			return *this + (-q);
		}

		Quaternion<T> operator*(const T& k) const {
			return Quaternion(k * a_, k * vec_);
		}

		Quaternion<T> operator*(const Quaternion<T>& q) const {
			return Quaternion(a_ * q.a_ - (vec_ * q.vec_), (a_ * q.vec_) + (q.a_ * vec_) + vec_.vec_product(q.vec_));
		}

		T sq_norm() const {
			return a_ * a_ + vec_[0] * vec_[0] + vec_[1] * vec_[1] + vec_[2] * vec_[2];
		}

		Quaternion<T> invert() const {
			return (1 / (this->sq_norm())) * Quaternion(a_, -vec_);
		}

		const T& operator[](int i) const {
			if (i == 0) return a_;
			else return vec_[i - 1];
		}

		T& operator[](int i) {
			if (i == 0) return a_;
			else return vec_[i - 1];
		}

		template <typename T1>
		friend std::ostream& operator<<(std::ostream&, const Quaternion<T1>&);
	private:
		T a_;
		Vector3<T> vec_;
};

template <typename T>
Quaternion<T> operator*(const T& k, const Quaternion<T>& q) {
	return q*k;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const Quaternion<T>& q) {
	out << q.a_ << " " << q.vec_;
}

template <typename T>
Vector3<T> rotate(const Vector3<T> vec, const Quaternion<T> q) {
	Quaternion<T> tmp = q * Quaternion<T>(0, vec);
	tmp = tmp * q.invert();
	return Vector3<T>(tmp[1], tmp[2], tmp[3]);
}
#endif // __QIATERNI__
