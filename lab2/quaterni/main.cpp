#include <iostream>
#include "vector3.h"
#include "quaternion.h"

int main() {
	Vector3<float> vec(1.f,0.f,1.f);
	Quaternion<float> qua(0.7071f, 0.7071f, 0.f, 0.f);
	std::cout << rotate(vec, qua) << std::endl;

	std::cout << "Enter 2 quaternions\n";
	Quaternion<float> qua1;
	std::cin >> qua[0] >> qua[1] >> qua[2] >> qua[3];
	std::cin >> qua1[0] >> qua1[1] >> qua1[2] >> qua1[3];

	std::cout << "-first, first - second, first + second, first * 5:\n";
	std::cout << -qua << std::endl;
	std::cout << qua - qua1 << std::endl;
	std::cout << qua + qua1 << std::endl;
	std::cout << qua * 5 << std::endl;
	return 0;
}
