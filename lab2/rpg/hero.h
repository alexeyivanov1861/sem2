#include <string>
#include <map>
#include <set>
#include <iostream>
#ifndef __HERO__
#define __HERO__

struct Ability {
	std::string name;
	std::string attribute_requirement;
	int attribute_value_requirement;
	int points_value_requirement;
};

struct Attribute_cmp {
	bool operator()(const Attribute& a, const Attribute& b) const {
		return a.name < b.name;
	}
}

class Hero {
	public:
		Hero(std::string = "John");
		friend std::ostream& operator<<(std::ostream& out, const Hero&);
		void lvlup(int = 1);
		int learn_ability(const Ability& a);
		int improve_attributes(std::string, int);
		const std::set<Attribute, Attribute_cmp>& get_attribute();
		int add_attribute(const Ability&);
	private:
		std::string name_;
		int level_;
		std::set<Attribute, Attribute_cmp> attribute_;
		std::set<Ability> ability_;
		int free_attribute_points_;
		int free_ability_points_;
};
#endif // __HERO__
