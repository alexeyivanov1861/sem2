#include "hero.h"

Hero::Hero(std::string name):
	name_(name),
	level_(0),
	free_attribute_points_(0),
	free_ability_points_(0) {
		attribute_.insert(Attribute("Strenght", 10));
		attribute_.insert(Attribute("Agility", 10));
		attribute_.insert(Attribute("Intelligence", 10));
		attribute_.insert(Attribute("Stamina", 10));
		attribute_.insert(Attribute("Charisma", 10));
}

std::ostream& operator<<(std::ostream& out, const Hero& h) {
	out << "Hero:\n";
	out << "Name: " << h.name_ << '\n';
	out << "Level: " << h.level_ << '\n';
	out << "Free attribute points: " << h.free_attribute_points_ << '\n';
	out << "Free ability points: " << h.free_ability_points_ << '\n';
	out << "Attributes: " << '\n';
	for (const auto& i : h.attribute_)
		out << i.first << " " << i.second << '\n';
	out << "Abilities: " << '\n';
	for (const auto& i : h.ability_)
		out << i.first << '\n';
	out << '\n';
	return out;
}

void Hero::lvlup(int up) {
	level_+= up;
	free_attribute_points_ += attribute_points_income_* up;
	free_ability_points_ += ability_points_income_ * up;
}

int Hero::learn_ability(const Ability& a) {
	if (free_attribute_points_ < a.second)
		return Hero::NOT_ENOUGH_POINTS;
	ability_.insert(a);
	return NOT_AN_ERROR;
}

const std::map<std::string, int>& Hero::get_attributes() {
	return attribute_;
}

int Hero::improve_attributes(std::string attribute, int value) {
	if (value < 0)
		return INCORRECT_ARGUMENT;
	std::map<std::string, int>::iterator it = attribute_.find(attribute);
	if (it == attribute_.end())
		return ATTRIBUTE_NOT_FOUND;
	int needed = value * (1 + attribute_cost_income_ * (it->second / Hero::attribute_step_of_income_));
	if (needed > Hero::free_attribute_points_)
		return NOT_ENOUGH_POINTS;
	if (it->second + value > max_attribute_value_)
		return OUT_OF_LIMITS;
	free_attribute_points_ -= needed;
	it->second += value;
	return NOT_AN_ERROR;
}

int Hero::add_attribute(const Ability& a) {
	attribute_.insert(a);
	return NOT_AN_ERROR;
}
