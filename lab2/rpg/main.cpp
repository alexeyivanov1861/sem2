#include "hero.h"
#include <iostream>
#include <string>
#include <vector>

void upgrade(Hero& hero) {
	std::cout << "Wellcome to the gym " << hero.name_ << '\n';
	std::cout << "There you can train and new something new\n";
	while (1) {
		std::cout << "1) Print info about hero\n";
		std::cout << "2) Set one of the global value\n";
		std::cout << "3) Level up\n";
		std::cout << "4) Up attributes\n";
		std::cout << "5) Add attribute\n";
		std::cout << "6) Learn skill\n";
		std::cout << "7) Print global values\n";
		std::cout << "8) Exit\n";
		int choice;
		std::cin >> choice;
		switch (choice) {
			case (1):
				std::cout << hero;
				break;
			case (2): {
				std::cout << "Choose value you want to set:\n";
				std::cout << "1) Start attribute value\n";
				std::cout << "2) Max attribute value\n";
				std::cout << "3) Attribute points income\n";
				std::cout << "4) Ability points income\n";
				std::cout << "5) Attribute cost income\n";
				std::cout << "6) Attriute_step_of_income\n";
				int choose_value;
				std::cin >> choose_value;
				std::cout << "Enter your value\n";
				int value;
				std::cin >> value;
				switch (choose_value) {
					case (1):
						Hero::start_attribute_value_= value;
						break;
					case (2):
						Hero::max_attribute_value_ = value;
						break;
					case (3):
						Hero::attribute_points_income_= value;
						break;
					case (4):
						Hero::ability_points_income_ = value;
						break;
					case (5):
						Hero::attribute_cost_income_ = value;
						break;
					case (6):
						Hero::attribute_step_of_income_ = value;
						break;
					default:
						break;
				}
				break;
				  }
			case (3): {
				std::cout << "How much levels you want to up?\n";
				int level;
				std::cin >> level;
				hero.lvlup(level);
				  }
				break;
			case (4): {
				std::cout << "What attribute you want to up?\n";
				std::vector<Ability> attribute_vec;
				for (auto& it : hero.get_attributes())
					attribute_vec.push_back(it);
				for (size_t i = 0; i < attribute_vec.size(); i++)
					std::cout << i + 1 << ") " << attribute_vec[i].first << '\n';
				unsigned attribute_number;
				std::cin >> attribute_number;
				if (attribute_number >= attribute_vec.size()) {
					std::cout << "Wrong number\n";
					break;
				}
				std::cout << "How much?\n";
				int attribute_up;
				std::cin >> attribute_up;
				int res = hero.improve_attributes(attribute_vec[attribute_number - 1].first, attribute_up);
				if (res == Hero::OUT_OF_LIMITS)
					std::cout << "Out of limits\n";
				else if (res == Hero::NOT_AN_ERROR)
					std::cout << "Not enough points\n";
				else if (res == Hero::NOT_AN_ERROR)
					std::cout << "Success\n";
				  }
				break;
			case (5): {
				std::cout << "Etner name of attribute and start value\n";
				Ability a;
				std::cin >> a.first >> a.second;
				hero.add_attribute(a);
				  }
				break;
			case (6): {
				std::cout << "Enter name of skill and cost of learning\n";
				Ability a;
				std::cin >> a.first >> a.second;
				if (hero.learn_ability(a) != Hero::NOT_AN_ERROR)
					std::cout << "Not enough points\n";
				else
					std::cout << "Success\n";
				  }
				break;
			case (7):
				std::cout << "1) Start attribute value " << Hero::start_attribute_value_ << '\n';
				std::cout << "2) Max attribute value " << Hero::max_attribute_value_ << '\n';
				std::cout << "3) Attribute points income " << Hero::attribute_points_income_ << '\n';;
				std::cout << "4) Ability points income " << Hero::ability_points_income_ << '\n';
				std::cout << "5) Attribute cost income " << Hero::attribute_cost_income_ << '\n';
				std::cout << "6) Attriute_step_of_income " << Hero::attribute_step_of_income_ << '\n'; ;
				break;
			case (8):
				return;
			default:
				break;
		}
	}
}

int main() {
	std::string name;
	std::cout << "Enter name of hero\n";
	std::cin >> name;
	Hero hero(name);
	upgrade(hero);
	return 0;
}
