#include "attribute.h"
#include <iostream>

int main() {
	Attribute a("Strenght", 10);
	std::cout << a.name_ << " " <<  a.value_ << '\n';
	for (auto& p : a.cost_)
		std::cout << p.first << " " << p.second << '\n';
	return 0;
}
