#include <vector>
#include <string>
#include <iostream>
#ifndef __ATTRIBUTE__
#define __ATTRIBUTE__
struct Attribute {
	std::string name_;
	int value_;
	std::vector<std::pair<int, int> > cost_;
	// <cost, level of change>

	Attribute(const std::string&, int);
	friend std::ostream& operator<<(std::ostream&, const Attribute&);
};
#endif //__ATTRIBUTE
