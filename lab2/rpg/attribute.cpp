#include "attribute.h"
#include <limits.h>

Attribute::Attribute(const std::string& name, int start_value)
       :name_(name),value_(start_value) {
	       cost_.push_back(std::make_pair(1, INT_MAX));
}	

std::ostream& operator<<(std::ostream& out, const Attribute& a) {
	out << a.name_ << ' ' << value_;
	out << "Cost:\n";
	int first = 0;
	for (size_t i = 0; i < a.cost_.size(); i++) {
		std::cout << "points on levels [" << first << " " << a[i].second << ")"
			<< "will cost you " << a[i].first << " points\n";
		first = a.second();
	}

}
