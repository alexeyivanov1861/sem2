#include "threadkeeper.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <thread>
#include <mutex>

void scalar_product(int *vector1, int *vector2, unsigned vector_size, int *result) {
    *result = 0;
    for (unsigned i = 0; i < vector_size; i++)
        *result += vector1[i] * vector2[i];
}


void getdata(int *&v1, int *&v2, unsigned &n, std::ifstream &in) {
    in >> n;

    v1 = new int[n];
    v2 = new int[n];

    for (unsigned i = 0; i < n; i++)
        in >> v1[i];
    for (unsigned i = 0; i < n; i++) 
        in >> v2[i];
}

int main() {
    unsigned thread_count = std::thread::hardware_concurrency();
    if (thread_count < 2)
        thread_count = 2;

    std::ifstream in("input.txt");
    int *v1, *v2;
    unsigned n;
    getdata(v1, v2, n, in);

    int *result = new int[thread_count];
    int h = n / thread_count;
    unsigned i;
    {
    ThreadKeeper keeper;
    for (i = 0; i < thread_count - 1; i++)
        keeper.make_new_thread(scalar_product, v1 + h * i, v2 + h * i, h, &result[i]);
    scalar_product(v1 + h * i, v2 + h * i, h + n - h * thread_count, &result[i]);
    }

    int r = 0;
    for (i = 0; i < thread_count; i++)
        r += result[i];
    std::cout << r << '\n';
    delete[] result;
    delete[] v1;
    delete[] v2;
    return 0;
}
/* time:
    SIZE = 1e7
    signle thread = 3.71 seconds
    4 thread = 2.37 seconds
*/
