#include <stdio.h>
#define N   10000000

int main(void) {
    printf("%d\n", N);
    int i;
    for (i = 0; i < N; i++)
        printf("%d ", 1);
    putchar('\n');
    for (i = 0; i < N; i++)
        printf("%d ", 1);
    putchar('\n');
    return 0;
}