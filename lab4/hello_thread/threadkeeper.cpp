#include "threadkeeper.h"

void ThreadKeeper::make_new_thread(thread_func_t f, int *vector1, int *vector2, unsigned n, int *result) {
        std::thread t(f, vector1, vector2, n, result);
        thread_.push_back(std::move(t));
}

ThreadKeeper::~ThreadKeeper() {
        for (auto &it : thread_)
            it.join();
    }