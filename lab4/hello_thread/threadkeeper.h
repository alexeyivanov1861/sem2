#ifndef THREADKEEPER_H
#define THREADKEEPER_H

#include <thread>
#include <vector>

typedef void (*thread_func_t)(int *vector1, int *vector2, unsigned n, int *result);

class ThreadKeeper {
    public:
    void make_new_thread(thread_func_t f, int *vector1, int *vector2, unsigned n, int *result);
    ~ThreadKeeper();
    private:
    std::vector<std::thread> thread_;
};

#endif //THREADKEEPER_H