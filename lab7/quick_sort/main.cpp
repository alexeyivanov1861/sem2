#include <iostream>
#include <fstream>
#include <future>
#include <list>
#include <algorithm>
#include <cstdlib>
#include <cmath>

#define N   (size_t)1e6


std::list<int> getdata() {
    srand(123);
    std::list<int> result;
    for (int i = 0; i < N; i++)
        result.push_back(rand() % 10000);
    return std::move(result);
}

std::list<int> quick_sort(std::list<int> a, int d = 0) {
    static const int max_d = log2(std::thread::hardware_concurrency());
    if (a.empty())
        return a;
    int m = a.front();
    auto mid_part_start = std::partition(a.begin(), a.end(), [m](int c){ return c < m; });
    auto right_part_start = std::partition(mid_part_start, a.end(), [m](int c){ return c == m; });

    std::list<int> left, mid;
    left.splice(left.end(), a, a.begin(), mid_part_start);
    mid.splice(mid.end(), a, mid_part_start, right_part_start);
    //now left - left, a - right

    std::launch launch_type = (d < max_d ? std::launch::async : std::launch::deferred);

    std::future<std::list<int>> sorted_left = async(launch_type, quick_sort, std::move(left), d + 1);
    a = quick_sort(a, d + 1);

    a.splice(a.begin(), mid);
    a.splice(a.begin(), sorted_left.get());

    return std::move(a);

}


int main() {
    std::list<int> a = quick_sort(getdata());
    for (auto it = a.begin(); it != a.end(); it++)
        std::cout << *it << ' ';
    std::cout << '\n';
    return 0;
}