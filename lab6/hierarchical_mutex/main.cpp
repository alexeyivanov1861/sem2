#include "hierarchical_mutex.h"
#include <iostream>

int main() {
   
    try {
        HierarchicalMutex m1(1000), m2(500), m3(100);
        m1.lock();
        m2.lock();
        m1.unlock();
        m2.unlock();
        std::cout << "Success!\n";
    } catch (const char * s) { std::cout << s << '\n'; }
    try {
        HierarchicalMutex m1(1000), m2(500), m3(100);
        m2.lock();
        m1.lock();
        m1.unlock();
        m2.unlock();
        std::cout << "Success!\n";
    } catch (const char * s) { std::cout << s << '\n'; }
    try {
        HierarchicalMutex m1(1000), m2(500), m3(100);
        m1.lock();
        m2.lock();
        m2.unlock();
        m1.unlock();
        std::cout << "Success!\n";
    } catch (const char * s) { std::cout << s << '\n'; }
    return 0;
}