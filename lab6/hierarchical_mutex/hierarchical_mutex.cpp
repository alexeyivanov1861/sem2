#include "hierarchical_mutex.h"

thread_local std::stack<int> HierarchicalMutex::priority_stack_ = std::stack<int>();

HierarchicalMutex::HierarchicalMutex(int p) 
    : priority_(p){
}

void HierarchicalMutex::lock() {
    if (!priority_stack_.empty() && priority_ > priority_stack_.top()) {
        priority_stack_ = std::stack<int>(); //clear stack
        throw "Wrong lock sequence";
    }
    m_.lock();
    priority_stack_.push(priority_);
}

void HierarchicalMutex::unlock() {
    if (priority_stack_.empty() || priority_stack_.top() != priority_) {
        priority_stack_ = std::stack<int>(); //clear stack
        throw "Wrong unlock sequence";
    }
    priority_stack_.pop();
    m_.unlock();
}