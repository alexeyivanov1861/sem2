#ifndef HIERARCHICAL_MUTEX_H
#define HIERARCHICAL_MUTEX_H

#include <mutex>
#include <stack>

class HierarchicalMutex {
    public:
        HierarchicalMutex(int p);
        void lock();
        void unlock();
    private:
        int priority_;
        static thread_local std::stack<int> priority_stack_;
        std::mutex m_;
};

#endif //HIERARCHICAL_MUTEX