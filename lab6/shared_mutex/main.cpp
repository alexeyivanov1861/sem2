#include "shared_mutex.h"
#include "threadkeeper.h"
#include <iostream>
#include <cstdlib>
#include <algorithm>

#define TEST_COUNT 1e3

SharedMutex m;

void read() {
    m.shared_lock(); //
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    m.shared_unlock(); //
}

void write() {
    m.lock();
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    m.unlock();
}

void task() {
    srand(0);
    int i;
    for (i = 0; i < TEST_COUNT; i++) {
        if (rand() % 10)
            read();
        else
            write();
    }
}

int main() {
    unsigned threads = std::max(2u, std::thread::hardware_concurrency());
    auto start = std::chrono::high_resolution_clock::now();
    unsigned i;
    {
    ThreadKeeper keeper;
    for (i = 0; i < threads; i++) 
        keeper.make_new_thread(task);
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n";
    return 0;
}


/*
TEST_SIZE: 1e3 THREADS_COUNT: 4
no shared: 7.25162s
yes shared: 2.3597s
single thread: 1.83312s
*/
