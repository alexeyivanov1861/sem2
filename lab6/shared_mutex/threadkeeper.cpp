#include "threadkeeper.h"

void ThreadKeeper::make_new_thread(thread_func_t f) {
        std::thread t(f);
        thread_.push_back(std::move(t));
}

ThreadKeeper::~ThreadKeeper() {
        for (auto &it : thread_)
            it.join();
    }