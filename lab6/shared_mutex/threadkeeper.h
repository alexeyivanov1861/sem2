#ifndef THREADKEEPER_H
#define THREADKEEPER_H

#include <thread>
#include <vector>

typedef void (*thread_func_t)();

class ThreadKeeper {
    public:
    void make_new_thread(thread_func_t f);
    ~ThreadKeeper();
    private:
    std::vector<std::thread> thread_;
};

#endif //THREADKEEPER_H