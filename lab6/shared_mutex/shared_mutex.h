#ifndef SHARED_MUTEX_H
#define SHARED_MUTEX_H

#include <mutex>
#include <chrono>
#include <thread>
#include <set>

class SharedMutex {
    public:
        void lock();
        void shared_lock();
        void unlock();
        void shared_unlock();
    private:
        std::mutex m_, reader_mutex_;
        std::set<std::thread::id> readers_;


};
#endif // SHARED_MUTEX_H