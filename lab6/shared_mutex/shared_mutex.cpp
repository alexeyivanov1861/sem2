#include "shared_mutex.h"

void SharedMutex::lock() {
    m_.lock();
    while (1) {
        reader_mutex_.lock();
        if (readers_.empty()) 
            break;
        reader_mutex_.unlock();
        std::this_thread::sleep_for(std::chrono::microseconds(50));
    }
    reader_mutex_.unlock();
}

void SharedMutex::shared_lock() {
    m_.lock(); //wait if someone writing
    reader_mutex_.lock();
    readers_.insert(std::this_thread::get_id());
    reader_mutex_.unlock();
    m_.unlock();
}

void SharedMutex::unlock() {
    m_.unlock();
}

void SharedMutex::shared_unlock() {
    reader_mutex_.lock();
    readers_.erase(std::this_thread::get_id());
    reader_mutex_.unlock();
}